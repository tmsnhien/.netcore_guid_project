//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\sonhung\Implemenation\IMS\App.Module\App.Module.Designer.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Notifications;
using DevExpress.ExpressApp.Objects;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Validation;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DomainComponents.Common;
using System.ComponentModel;
using YouSoft.Vn.XAFCustom.Core.Custom;


namespace App.Module
{
    partial class App_Module
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        public override bool NeedCustomNotificationColumns => true;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // App_Module
            // 
            AdditionalExportedTypes.Add(typeof(XPBaseObject));
            AdditionalExportedTypes.Add(typeof(PersistentBase));
            AdditionalExportedTypes.Add(typeof(XPObject));
            AdditionalExportedTypes.Add(typeof(XPCustomObject));
            AdditionalExportedTypes.Add(typeof(BaseObject));
            AdditionalExportedTypes.Add(typeof(ReportDataV2));
            AdditionalExportedTypes.Add(typeof(XtraReportData));
            AdditionalExportedTypes.Add(typeof(OidGenerator));
            AdditionalExportedTypes.Add(typeof(ServerPrefix));
            AdditionalExportedTypes.Add(typeof(Event));
            AdditionalExportedTypes.Add(typeof(IPersistentEvent));
            AdditionalExportedTypes.Add(typeof(IPersistentResource));
            RequiredModuleTypes.Add(typeof(SystemModule));
            RequiredModuleTypes.Add(typeof(BusinessClassLibraryCustomizationModule));
            RequiredModuleTypes.Add(typeof(ConditionalAppearanceModule));
            RequiredModuleTypes.Add(typeof(ValidationModule));
            RequiredModuleTypes.Add(typeof(ReportsModuleV2));
            RequiredModuleTypes.Add(typeof(YsvCoreModule));
            RequiredModuleTypes.Add(typeof(NotificationsModule));
            
        }
        #endregion
    }
}
