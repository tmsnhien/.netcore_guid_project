﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\sonhung\Implemenation\IMS\App.Module\Modules\M01_Common\Entities\Employee.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using YouSoft.Vn.XAFCustom.Core.Entities;
using YouSoft.Vn.XAFCustom.Core.Extensions;
using YouSoft.Vn.XAFCustom.Supports.Attributes.Validation;


namespace ModuleBase.Entities
{
    [DefaultClassOptions]
    [XafDisplayName("Nhân viên")]
    [ImageName("BO_Employee")]
    //[CreatableItem]
    [VisibleInDashboards]
    [VisibleInReports]
    [NavigationItem("ModuleBase.Entities")]
    [LookupEditorMode(LookupEditorMode.AllItemsWithSearch)]

    public partial class Employee : YsvUser
    {
        public Employee(Session session)
                   : base(session)
        {
        }

        protected override void OnSavingOnly()
        {
            this.GenerateCodeIfEmpty("NV", 4, string.Empty);
            base.OnSavingOnly();
        }
    }
}