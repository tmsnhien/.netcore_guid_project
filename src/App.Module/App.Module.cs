﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\sonhung\Implemenation\IMS\App.Module\App.Module.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using App.Module.Modules.Inventory;
using App.Module.Properties;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Validation;
using ModuleBase.Entities;
using System.Drawing;
using YouSoft.Vn.XAFCustom.Core.Entities;
using YouSoft.Vn.XAFCustom.Supports;
using YouSoft.Vn.XAFCustom.Supports.Attributes.Misc;
using YouSoft.Vn.XAFCustom.Supports.Custom;
using YouSoft.Vn.XAFCustom.Supports.Extensions;
using YouSoft.Vn.XAFCustom.Supports.Services;
using static YouSoft.Vn.XAFCustom.Supports.CoreApp;

namespace App.Module
{
    public sealed partial class App_Module : YsvMainModule
    {
        public static bool ServiceMode;
        public App_Module() => InitializeComponent();

        protected override Icon ProductIcon => Resources.Main;

        protected override Image ProductImage => Resources.ProductImage;


        public override void InitEmployee(IObjectSpace os)
        {
            YsvUser.InitPermission<Employee>(os);
            
        }
        protected override void Register(ModuleEntityService service)
        {
            base.Register(service);
            service.Register<Employee>()
                .Register<Product>()
                .Register<AppSettings>();
        }
        public override void OnSetupComplete(XafApplication application)
        {
            base.OnSetupComplete(application);
            YsvAppSettings.Init<AppSettings>();
        }
    }
}