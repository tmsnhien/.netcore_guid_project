﻿using App.BlazorServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using ModuleBase.Entities;
using YouSoft.Vn.XAFCustom.BlazorServer.Core;
using YouSoft.Vn.XAFCustom.Core.Entities;

namespace BlazorApp.BlazorServer
{
    public class Startup:YsvStartup<App_BlazorServer, YsvRole, Employee>
    {   public Startup(IConfiguration configuration): base(configuration) {
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);
            var supportedCultures = new[] { "vi"};
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);

            app.UseRequestLocalization(localizationOptions);
        }
    }
}
