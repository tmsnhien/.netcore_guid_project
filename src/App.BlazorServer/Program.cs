﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Blazor.DesignTime;
using DevExpress.ExpressApp.Blazor.Services;
using DevExpress.ExpressApp.Design;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using YouSoft.Vn.XAFCustom.BlazorServer.Core;

namespace BlazorApp.BlazorServer
{

    public class Program : YsvProgram<Startup>{
        public static int Main(string[] args) {
            return Startup(args);
        }
    }
}
