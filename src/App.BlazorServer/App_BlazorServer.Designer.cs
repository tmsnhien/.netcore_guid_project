﻿using App.Module;
using DevExpress.ExpressApp.Office;
using DevExpress.ExpressApp.TreeListEditors;
using YouSoft.Vn.XAFCustom.BlazorServer;
using YouSoft.Vn.XAFCustom.Core.Custom;

namespace App.BlazorServer
{
    partial class App_BlazorServer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            mainModule = new App_Module();
            this.systemBlazorModule = new DevExpress.ExpressApp.Blazor.SystemModule.SystemBlazorModule();
            this.ysvCoreModule = new YsvCoreModule();
            this.ysvBlazorModule = new YsvBlazorModule();
            this.securityModule = new DevExpress.ExpressApp.Security.SecurityModule();
            this.conditionalAppearanceModule = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
            this.fileAttachmentsBlazorModule = new DevExpress.ExpressApp.FileAttachments.Blazor.FileAttachmentsBlazorModule();
            this.reportsModuleV2 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
            this.reportsBlazorModuleV2 = new DevExpress.ExpressApp.ReportsV2.Blazor.ReportsBlazorModuleV2();
            this.validationModule = new DevExpress.ExpressApp.Validation.ValidationModule();
            this.validationBlazorModule = new DevExpress.ExpressApp.Validation.Blazor.ValidationBlazorModule();
            this.viewVariantsModule = new DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule();
            this.reportsModuleV2.EnableInplaceReports = true;
            this.reportsModuleV2.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
            this.reportsModuleV2.ShowAdditionalNavigation = false;
            this.reportsModuleV2.ReportStoreMode = DevExpress.ExpressApp.ReportsV2.ReportStoreModes.XML;
            officeModule1 = new OfficeModule();
        treeListEditorsModuleBase1 = new TreeListEditorsModuleBase();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.Modules.Add(mainModule);
            this.Modules.Add(this.systemBlazorModule);
            this.Modules.Add(this.ysvCoreModule);
            this.Modules.Add(this.ysvBlazorModule);
            this.Modules.Add(this.securityModule);
            this.Modules.Add(this.conditionalAppearanceModule);
            this.Modules.Add(this.fileAttachmentsBlazorModule);
            this.Modules.Add(this.reportsModuleV2);
            this.Modules.Add(this.reportsBlazorModuleV2);
            this.Modules.Add(this.validationModule);
            this.Modules.Add(this.validationBlazorModule);
            this.Modules.Add(this.viewVariantsModule);
            this.Modules.Add(this.officeModule1);
            this.Modules.Add(this.treeListEditorsModuleBase1);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

        #endregion
        private DevExpress.ExpressApp.Blazor.SystemModule.SystemBlazorModule systemBlazorModule;
        private DevExpress.ExpressApp.Security.SecurityModule securityModule;
        private DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule conditionalAppearanceModule;
        private DevExpress.ExpressApp.FileAttachments.Blazor.FileAttachmentsBlazorModule fileAttachmentsBlazorModule;
        private DevExpress.ExpressApp.ReportsV2.ReportsModuleV2 reportsModuleV2;
        private DevExpress.ExpressApp.ReportsV2.Blazor.ReportsBlazorModuleV2 reportsBlazorModuleV2;
        private DevExpress.ExpressApp.Validation.ValidationModule validationModule;
        private DevExpress.ExpressApp.Validation.Blazor.ValidationBlazorModule validationBlazorModule;
        private DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule viewVariantsModule;
        private YsvCoreModule ysvCoreModule;
        private App_Module mainModule;
        private OfficeModule officeModule1;
        private TreeListEditorsModuleBase treeListEditorsModuleBase1;
        YsvBlazorModule ysvBlazorModule;

    }
}
