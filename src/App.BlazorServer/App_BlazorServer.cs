﻿using YouSoft.Vn.XAFCustom.BlazorServer;

namespace App.BlazorServer
{
    public partial class App_BlazorServer : YsvBlazorApplication {
        public App_BlazorServer() {
            InitializeComponent();
            AfterInitializedComponent();
        }
    }
}
