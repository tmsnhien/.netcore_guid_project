﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="WebApp.LoginPage" EnableViewState="false" CodeBehind="Login.aspx.cs" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v20.2, Version=20.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.ExpressApp.Web.Controls" TagPrefix="cc4" %>
<%@ Register assembly="DevExpress.Web.v20.2, Version=20.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>Logon</title>
    <link rel="stylesheet" href="\css\style.css">
    <!--link rel="stylesheet" href="\css\project.css"-->
     <link rel="shortcut icon" href="/images/favicon.ico" />
</head>
<body class="Dialog" >
<div id="PageContent" class="PageContent DialogPageContent">
    <form id="form1" runat="server">
        <cc4:ASPxProgressControl ID="ProgressControl" runat="server"/>
        <div id="Content" runat="server"/>
    </form>
</div>
</body>
</html>