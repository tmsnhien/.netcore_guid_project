﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\DefaultVerticalTemplateContent1.ascx.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.Web.Controls;
using DevExpress.ExpressApp.Web.Templates.ActionContainers;
using System;
using System.Web.UI.WebControls;
using YouSoft.Vn.XAFCustom.Web.App;


namespace App.Web
{
    public partial class DefaultVerticalTemplateContent1 : YsvTemplateContent
    {
        public override void BeginUpdate()
        {
            base.BeginUpdate();
            SAC.BeginUpdate();
            mainMenu.BeginUpdate();
            SearchAC.BeginUpdate();
        }

        public override void EndUpdate()
        {
            SAC.EndUpdate();
            mainMenu.EndUpdate();
            SearchAC.EndUpdate();
            base.EndUpdate();
        }

        protected override HyperLink GetLogoLink() => LogoLink;

        protected override ActionContainerHolder GetMainMenu() => mainMenu;

        protected override Panel GetNavigationPanel() => navigation;

        protected override XafPopupWindowControl GetPopupWindowControl() => PopupWindowControl;

        protected override ActionContainerHolder GetSecurityActionContainerHolder() => SAC;

        protected override ThemedImageControl GetThemedImageControl() => TIC;

        protected override ViewSiteControl GetViewSiteControl() => VSC;
    }
}
