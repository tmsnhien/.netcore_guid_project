﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\Global.asax.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using App.Web;
using DevExpress.ExpressApp.Web;
using System;
using YouSoft.Vn.XAFCustom.Web.App;


namespace WebApp
{
    public class Global : YsvHttpApplication
    {
        protected override YsvWebApplication CreateApplicationInstance() => new WebAspNetApplication();

        protected override void CustomTemplate()
        {
            base.CustomTemplate();
            WebApplication.Instance.Settings.DefaultVerticalTemplateContentPath = $"{nameof(DefaultVerticalTemplateContent1)}.ascx";
            DefaultVerticalTemplateContent1.ClearSizeLimit();

        }
    }
}