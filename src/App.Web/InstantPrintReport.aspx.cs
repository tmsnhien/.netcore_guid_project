﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\InstantPrintReport.aspx.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.IO;


namespace YouSoft.Vn.XAFCustom.Web.Pages
{
    public partial class InstantPrintReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var reportDataHandle = Request.QueryString["reportContainerHandle"];
            var module = ReportsModuleV2.FindReportsModule(ApplicationReportObjectSpaceProvider.ContextApplication.Modules);
            if (!string.IsNullOrEmpty(reportDataHandle) && (module != null))
            {
                XtraReport report = null;
                try
                {
                    report = ReportDataProvider.ReportsStorage
                                 .GetReportContainerByHandle(reportDataHandle)
                                 .Report;
                    module.ReportsDataSourceHelper
                        .SetupBeforePrint(report, null, null, false, null, false);
                    using (var ms = new MemoryStream())
                    {
                        report.CreateDocument();
                        var options = new PdfExportOptions();
                        options.ShowPrintDialogOnOpen = true;
                        report.ExportToPdf(ms, options);
                        ms.Seek(0, SeekOrigin.Begin);
                        var reportContent = ms.ToArray();
                        Response.ContentType = "application/pdf";
                        Response.Clear();
                        Response.OutputStream.Write(reportContent, 0, reportContent.Length);
                        Response.End();
                    }
                }
                finally
                {
                    if (report != null)
                    {
                        report.Dispose();
                    }
                }
            }
        }
    }
}