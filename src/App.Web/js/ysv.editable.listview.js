﻿var _fieldName = '';

function OnEditClick(s, e) {
    var srcElement = e.htmlEvent.srcElement;
    _fieldName = srcElement.getAttribute('FieldName');
    s.StartEditRow(e.visibleIndex);
}

function OnEndCallback(s, e) {
    if (s.IsEditing()) {
        var editor = s.GetEditor(_fieldName);
        if(!editor)
        editor.Focus();
    }
}