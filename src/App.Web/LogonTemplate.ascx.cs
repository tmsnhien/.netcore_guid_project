﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\LogonTemplate.ascx.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Web.Controls;
using DevExpress.ExpressApp.Web.Templates;
using System.Collections.Generic;


namespace WebApp
{
    public partial class LogonTemplate : TemplateContent, IXafPopupWindowControlContainer
    {
        public static bool HasLicenseProblem = false;

        public override IActionContainer DefaultContainer => null;

        public override object ViewSiteControl => viewSiteControl;

        public XafPopupWindowControl XafPopupWindowControl
        {
            get;
        }

        public override void SetStatus(ICollection<string> statusMessages)
        {
        }

        public override void BeginUpdate()
        {
            base.BeginUpdate();
            PopupActions.BeginUpdate();
        }

        public override void EndUpdate()
        {
            PopupActions.EndUpdate();
            base.EndUpdate();
        }
    }
}