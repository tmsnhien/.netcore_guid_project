﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\Error.aspx.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.ExpressApp.Web.TestScripts;
using System;
using System.Web.UI;


namespace WebApp
{
    public partial class ErrorPage : Page
    {
        protected override void InitializeCulture()
        {
            if (WebApplication.Instance != null)
                WebApplication.Instance.InitializeCulture();
        }

        private void Page_Load(object sender, EventArgs e)
        {
            var testScriptsManager = new TestScriptsManager(Page);
            testScriptsManager.RegisterControl(JSLabelTestControl.ClassName,
                                                   "FormCaption",
                                                   TestControlType.Field,
                                                   "FormCaption");
            testScriptsManager.RegisterControl(JSLabelTestControl.ClassName,
                                                   nameof(DescriptionTextBox),
                                                   TestControlType.Field,
                                                   "Description");
            testScriptsManager.RegisterControl(JSDefaultTestControl.ClassName,
                                                   nameof(ReportButton),
                                                   TestControlType.Action,
                                                   "Report");
            testScriptsManager.AllControlRegistered();
            ClientScript.RegisterStartupScript(GetType(),
                                                   "EasyTest",
                                                   testScriptsManager.GetScript(),
                                                   true);
            if (WebApplication.Instance != null)
                ApplicationTitle.Text = WebApplication.Instance.Title;
            else
                ApplicationTitle.Text = "No application";
            Header.Title = "Application Error - " + ApplicationTitle.Text;

            var errorInfo = ErrorHandling.GetApplicationError();
            if (errorInfo != null)
            {
                if (ErrorHandling.CanShowDetailedInformation)
                    DetailsText.Text = errorInfo.GetTextualPresentation(true);
                else
                    Details.Visible = false;
                ReportResult.Visible = false;
                ReportForm.Visible = ErrorHandling.CanSendAlertToAdmin;
            }
            else
            {
                ErrorPanel.Visible = false;
            }
        }

        protected void ReportButton_Click(object sender, EventArgs e)
        {
            var errorInfo = ErrorHandling.GetApplicationError();
            if (errorInfo != null)
            {
                ErrorHandling.SendAlertToAdmin(errorInfo.Id,
                                                   DescriptionTextBox.Text,
                                                   errorInfo.Exception.Message);
                ErrorHandling.ClearApplicationError();
                ApologizeMessage.Visible = false;
                ReportForm.Visible = false;
                Details.Visible = false;
                ReportResult.Visible = true;
            }
        }

        protected void NavigateToStart_Click(object sender, EventArgs e) => WebApplication.Instance
                                                                                .LogOff();

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            Load += new EventHandler(Page_Load);
            PreRender += new EventHandler(ErrorPage_PreRender);
        }

        public override void Dispose()
        {
            Load -= new EventHandler(Page_Load);
            PreRender -= new EventHandler(ErrorPage_PreRender);
            base.Dispose();
        }

        private void ErrorPage_PreRender(object sender, EventArgs e) => RegisterThemeAssemblyController.RegisterThemeResources((Page)sender);
        #endregion
    }
}