﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\omina-qldn\Implemenation\IMS\App.Web\WebModuleComponents.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp;
using System;
using YouSoft.Vn.XAFCustom.Supports;
using YouSoft.Vn.XAFCustom.Web.Controllers.Formatter;


namespace App.Web
{
    public class WebAppModuleComponents
    {
        public static void OnInitialized()
        {
            //FilterByDateController.AddNumOfDaysBefore = 30;
            //FilterByDateController.AddNumOfDaysBefore = 30;
            WebFormatListViewController.SkipIndexColumnForNotification = true;
            WebFormatListViewController.PagerSize = 10;
            CoreApp.OptionsBehaviours.LoadProvince = true;
            YsvWebNotificationsDialogViewController.Height = 600;
            YsvWebNotificationsDialogViewController.Width = 1400;
            //XafPopupWindowControl.DefaultHeight = Unit.Percentage(99);
            //XafPopupWindowControl.DefaultWidth = Unit.Percentage(99);
            WebFormatListViewController.NewItemRowPosition = NewItemRowPosition.Top;
        }
    }
}
