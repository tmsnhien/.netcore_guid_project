﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\sonhung\Implemenation\IMS\App.Web\WebApplication.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using App.Module;
using App.Web;
using DevExpress.ExpressApp.ConditionalAppearance;

using DevExpress.ExpressApp.FileAttachments.Web;
using DevExpress.ExpressApp.Notifications;
using DevExpress.ExpressApp.Notifications.Web;
using DevExpress.ExpressApp.Objects;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.ReportsV2.Web;
using DevExpress.ExpressApp.Scheduler;
using DevExpress.ExpressApp.Scheduler.Web;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Validation;
using DevExpress.ExpressApp.Validation.Web;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.SystemModule;
using System.Drawing;
using System.Web.UI.WebControls;
using YouSoft.Vn.XAFCustom.Core.Custom;
using YouSoft.Vn.XAFCustom.Supports.Attributes.Misc;
using YouSoft.Vn.XAFCustom.Supports.Custom;
using YouSoft.Vn.XAFCustom.Web;
using YouSoft.Vn.XAFCustom.Web.App;


namespace WebApp
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/DevExpressExpressAppWebWebApplicationMembersTopicAll.aspx
    public partial class WebAspNetApplication : YsvWebApplication
    {
      
        static WebAspNetApplication()
        {
        }
        AuthenticationStandard authenticationStandard1;
        BusinessClassLibraryCustomizationModule businessClassLibraryCustomizationModule1;

        ConditionalAppearanceModule conditionalAppearanceModule1;
        App_Module coreModule1;

        //DashboardsAspNetModule dashboardsAspNetModule1;
        //DashboardsModule dashboardsModule1;
        FileAttachmentsAspNetModule fileAttachmentsAspNetModule1;

        SystemModule module1;
        SystemAspNetModule module2;
        NotificationsModule notificationsModule1;

        ReportsAspNetModuleV2 reportsAspNetModuleV21;
        ReportsModuleV2 reportsModuleV21;
        SchedulerAspNetModule schedulerAspNetModule1;
        SchedulerModuleBase schedulerModuleBase1;
        SecurityModule securityModule1;
        SecurityStrategyComplex securityStrategyComplex1;
        ValidationAspNetModule validationAspNetModule;
        ValidationModule validationModule;
        private NotificationsAspNetModule notificationsAspNetModule1;
        private YsvWebModule ysvWebModule1;
        private DevExpress.ExpressApp.CloneObject.CloneObjectModule cloneObjectModule1;
        private DevExpress.ExpressApp.Dashboards.Web.DashboardsAspNetModule dashboardsAspNetModule1;
        private DevExpress.ExpressApp.Dashboards.DashboardsModule dashboardsModule1;
       
        YsvCoreModule ysvCoreModule1;

        public WebAspNetApplication()
        {

            InitializeComponent();
            AfterInitializedComponent();
        }

        protected override void OnInitialized() => base.OnInitialized();

        void InitializeComponent()
        {
            this.module1 = new DevExpress.ExpressApp.SystemModule.SystemModule();
            this.module2 = new DevExpress.ExpressApp.Web.SystemModule.SystemAspNetModule();
            this.validationModule = new DevExpress.ExpressApp.Validation.ValidationModule();
            this.validationAspNetModule = new DevExpress.ExpressApp.Validation.Web.ValidationAspNetModule();
            this.businessClassLibraryCustomizationModule1 = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
            this.conditionalAppearanceModule1 = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
            this.reportsModuleV21 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
            this.notificationsModule1 = new DevExpress.ExpressApp.Notifications.NotificationsModule();
            this.schedulerAspNetModule1 = new DevExpress.ExpressApp.Scheduler.Web.SchedulerAspNetModule();
            this.schedulerModuleBase1 = new DevExpress.ExpressApp.Scheduler.SchedulerModuleBase();
            this.reportsAspNetModuleV21 = new DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2();
            this.fileAttachmentsAspNetModule1 = new DevExpress.ExpressApp.FileAttachments.Web.FileAttachmentsAspNetModule();
            this.ysvCoreModule1 = new YouSoft.Vn.XAFCustom.Core.Custom.YsvCoreModule();
            this.notificationsAspNetModule1 = new DevExpress.ExpressApp.Notifications.Web.NotificationsAspNetModule();
            this.ysvWebModule1 = new YouSoft.Vn.XAFCustom.Web.YsvWebModule();
            this.cloneObjectModule1 = new DevExpress.ExpressApp.CloneObject.CloneObjectModule();
            this.securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
            this.coreModule1 = new App.Module.App_Module();
            this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
            this.authenticationStandard1 = new DevExpress.ExpressApp.Security.AuthenticationStandard();
            this.dashboardsAspNetModule1 = new DevExpress.ExpressApp.Dashboards.Web.DashboardsAspNetModule();
            this.dashboardsModule1 = new DevExpress.ExpressApp.Dashboards.DashboardsModule();
            
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // validationModule
            // 
            this.validationModule.AllowValidationDetailsAccess = true;
            this.validationModule.IgnoreWarningAndInformationRules = false;
            // 
            // reportsModuleV21
            // 
            this.reportsModuleV21.EnableInplaceReports = true;
            this.reportsModuleV21.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
            // 
            // notificationsModule1
            // 
            this.notificationsModule1.CanAccessPostponedItems = true;
            this.notificationsModule1.NotificationsRefreshInterval = System.TimeSpan.Parse("00:01:00");
            this.notificationsModule1.NotificationsStartDelay = System.TimeSpan.Parse("00:00:05");
            this.notificationsModule1.ShowDismissAllAction = true;
            this.notificationsModule1.ShowNotificationsWindow = false;
            this.notificationsModule1.ShowRefreshAction = false;
            // 
            // cloneObjectModule1
            // 
            this.cloneObjectModule1.ClonerType = null;
            // 
            // securityStrategyComplex1
            // 
            this.securityStrategyComplex1.AllowAnonymousAccess = false;
            this.securityStrategyComplex1.Authentication = this.authenticationStandard1;
            this.securityStrategyComplex1.PermissionsReloadMode = DevExpress.ExpressApp.Security.PermissionsReloadMode.CacheOnFirstAccess;
            this.securityStrategyComplex1.RoleType = typeof(YouSoft.Vn.XAFCustom.Core.Entities.YsvRole);
            this.securityStrategyComplex1.UseOptimizedPermissionRequestProcessor = false;
            this.securityStrategyComplex1.UsePermissionRequestProcessor = false;
            this.securityStrategyComplex1.UserType = typeof(ModuleBase.Entities.Employee);
            // 
            // authenticationStandard1
            // 
            this.authenticationStandard1.LogonParametersType = typeof(DevExpress.ExpressApp.Security.AuthenticationStandardLogonParameters);
            // 
            // dashboardsModule1
            // 
            this.dashboardsModule1.DashboardDataType = typeof(DevExpress.Persistent.BaseImpl.DashboardData);
            // 
            // WebAspNetApplication
            // 
            this.ApplicationName = "IMS";
            this.CheckCompatibilityType = DevExpress.ExpressApp.CheckCompatibilityType.DatabaseSchema;
            this.Modules.Add(this.module1);
            this.Modules.Add(this.module2);
            this.Modules.Add(this.businessClassLibraryCustomizationModule1);
            this.Modules.Add(this.conditionalAppearanceModule1);
            this.Modules.Add(this.validationModule);
            this.Modules.Add(this.reportsModuleV21);
            this.Modules.Add(this.notificationsModule1);
            this.Modules.Add(this.securityModule1);
            this.Modules.Add(this.validationAspNetModule);
            this.Modules.Add(this.schedulerModuleBase1);
            this.Modules.Add(this.schedulerAspNetModule1);
            this.Modules.Add(this.reportsAspNetModuleV21);
            this.Modules.Add(this.fileAttachmentsAspNetModule1);
            this.Modules.Add(this.cloneObjectModule1);
            this.Modules.Add(this.ysvCoreModule1);
            this.Modules.Add(this.dashboardsModule1);
            this.Modules.Add(this.coreModule1);
            this.Modules.Add(this.notificationsAspNetModule1);
            this.Modules.Add(this.ysvWebModule1);
            this.Modules.Add(this.dashboardsAspNetModule1);
            this.Security = this.securityStrategyComplex1;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        public override void AfterInitializedComponent()
        {
            base.AfterInitializedComponent();
            //MainModule//.Add<WebFillDataListToExcelTemplateController>()
            //.Add<WebCustomNotificationColumnsController>()
            //.Add<InlineEditListViewController>();
            WebAppModuleComponents.OnInitialized();
        }

        protected override void OnSetupComplete()
        {
            base.OnSetupComplete();

            NameFormatterAttribute.DefaultFontStyle = FontStyle.Regular;
            NameFormatterAttribute.DefaultStyle = PredefinedStyle.WarningLabel;
            CodeFormatterAttribute.DefaultStyle = PredefinedStyle.InfoLabel;
            CodeFormatterAttribute.DefaultFontStyle = FontStyle.Regular;
        }

        protected override void CustomMainModule(YsvMainModule mainModule)
        {
            base.CustomMainModule(mainModule);

        }


        protected override IViewUrlManager CreateViewUrlManager() => new ViewUrlManager();

    }

}