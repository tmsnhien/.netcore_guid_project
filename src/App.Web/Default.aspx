﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="WebApp.Default" EnableViewState="false"
ValidateRequest="false" CodeBehind="Default.aspx.cs" UICulture="vi" Culture="vi-VN" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v20.2, Version=20.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.ExpressApp.Web.Controls" TagPrefix="cc4" %>
<%@ Register assembly="DevExpress.Web.v20.2, Version=20.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Main Page</title>
    <meta http-equiv="Expires" content="0"/>
    <link rel="shortcut icon" href="/images/favicon.ico" />
    
    <!--<link rel="stylesheet" href="\css\project.css">-->
    <!--style type="text/css">
        
        .dxm-item.accountItem.dxm-subMenu .dx-vam { padding-left: 10px; }

        .dxm-item.accountItem.dxm-subMenu .dxm-image.dx-vam {
            -moz-border-radius: 32px;
            -webkit-border-radius: 32px;
            border-radius: 32px;
            max-height: 32px;
            max-width: 32px;
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .Layout.LayoutEditMode td.GroupContent
        {
            border-color: Black;
            border-style: solid;
            border-width: 1px;
        }
        
        
    </style-->
    <link rel="stylesheet" href="\css\style.css">
    <script type="text/javascript" src="\js\ysv.editable.listview.js"></script>

</head>
<body class="VerticalTemplate">
<form id="form2" runat="server">
    <cc4:ASPxProgressControl ID="ProgressControl" runat="server"/>
    <div runat="server" id="Content"/>
</form>
     <script>
        if (window.ASPx && ASPxClientUtils.webKitFamily && ASPxClientUtils.browserVersion >= 75) {
            ASPx.SSLSecureBlankUrl = "about:blank";
        }
    </script>
</body>
</html>