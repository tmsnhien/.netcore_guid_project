//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\27_Anhtuyen.popga\Implemenation\IMS\App.Win\WinApplication.Designer.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using App.Module;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.FileAttachments.Win;
using DevExpress.ExpressApp.Notifications;
using DevExpress.ExpressApp.Notifications.Win;
using DevExpress.ExpressApp.Objects;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.ReportsV2.Win;
using DevExpress.ExpressApp.Scheduler;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Validation;
using DevExpress.ExpressApp.Validation.Win;
using DevExpress.ExpressApp.ViewVariantsModule;
using DevExpress.ExpressApp.Win.SystemModule;
using System.ComponentModel;
using YouSoft.Vn.XAFCustom.Core.Custom;
using YouSoft.Vn.XAFCustom.Win;

namespace App.Win
{
    partial class ImsWindowsFormsApplication
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent()
        {
            DevExpress.ExpressApp.Security.SecurityModule securityModule1;
            this.systemModule = new DevExpress.ExpressApp.SystemModule.SystemModule();
            this.systemWindowsFormModule = new DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule();
            this.businessClassLibraryCustomizationModule1 = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
            this.validationModule1 = new DevExpress.ExpressApp.Validation.ValidationModule();
            this.validationWindowsFormsModule1 = new DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule();
            this.conditionalAppearanceModule1 = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
            this.systemWindowsFormsModule1 = new DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule();
            this.schedulerWindowsFormsModule1 = new DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule();
            this.schedulerModuleBase1 = new DevExpress.ExpressApp.Scheduler.SchedulerModuleBase();
            this.fileAttachmentsWindowsFormsModule1 = new DevExpress.ExpressApp.FileAttachments.Win.FileAttachmentsWindowsFormsModule();
            this.notificationsWindowsFormsModule1 = new DevExpress.ExpressApp.Notifications.Win.NotificationsWindowsFormsModule();
            this.notificationsModule1 = new DevExpress.ExpressApp.Notifications.NotificationsModule();
            this.reportsModuleV21 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
            this.reportsWindowsFormsModuleV21 = new DevExpress.ExpressApp.ReportsV2.Win.ReportsWindowsFormsModuleV2();
            this.coreModule = new App.Module.App_Module();
            this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
            this.authenticationStandard1 = new DevExpress.ExpressApp.Security.AuthenticationStandard();
            this.YsvCoreModule1 = new YouSoft.Vn.XAFCustom.Core.Custom.YsvCoreModule();
            this.ysvWinModule1 = new YouSoft.Vn.XAFCustom.Win.YsvWinModule();
            this.viewVariantsModule1 = new DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule();
            this.cloneObjectModule1 = new DevExpress.ExpressApp.CloneObject.CloneObjectModule();
         
         
           
            securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // validationModule1
            // 
            this.validationModule1.AllowValidationDetailsAccess = true;
            this.validationModule1.IgnoreWarningAndInformationRules = false;
            // 
            // notificationsModule1
            // 
            this.notificationsModule1.CanAccessPostponedItems = true;
            this.notificationsModule1.NotificationsRefreshInterval = System.TimeSpan.Parse("00:05:00");
            this.notificationsModule1.NotificationsStartDelay = System.TimeSpan.Parse("00:00:05");
            this.notificationsModule1.ShowDismissAllAction = true;
            this.notificationsModule1.ShowNotificationsWindow = true;
            this.notificationsModule1.ShowRefreshAction = true;
            // 
            // reportsModuleV21
            // 
            this.reportsModuleV21.EnableInplaceReports = true;
            this.reportsModuleV21.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
            // 
            // coreModule
            // 
            this.coreModule.AdditionalControllerTypes.Add(typeof(DevExpress.ExpressApp.Notifications.NotificationsDialogViewController));
            this.coreModule.AdditionalControllerTypes.Add(typeof(DevExpress.ExpressApp.Notifications.NotificationsOnCommitController));
            this.coreModule.AdditionalControllerTypes.Add(typeof(DevExpress.ExpressApp.Notifications.NotificationsMessageListViewController));
            this.coreModule.AdditionalControllerTypes.Add(typeof(DevExpress.ExpressApp.Notifications.UpdateNotificationsSourceListViewController));
            // 
            // securityStrategyComplex1
            // 
            this.securityStrategyComplex1.AllowAnonymousAccess = false;
            this.securityStrategyComplex1.Authentication = this.authenticationStandard1;
            this.securityStrategyComplex1.PermissionsReloadMode = DevExpress.ExpressApp.Security.PermissionsReloadMode.CacheOnFirstAccess;
            this.securityStrategyComplex1.RoleType = typeof(YouSoft.Vn.XAFCustom.Core.Entities.YsvRole);
            this.securityStrategyComplex1.UseOptimizedPermissionRequestProcessor = false;
            this.securityStrategyComplex1.UsePermissionRequestProcessor = false;
            this.securityStrategyComplex1.UserType = typeof(ModuleBase.Entities.Employee);
            // 
            // authenticationStandard1
            // 
            this.authenticationStandard1.LogonParametersType = typeof(DevExpress.ExpressApp.Security.AuthenticationStandardLogonParameters);
            // 
            // cloneObjectModule1
            // 
            this.cloneObjectModule1.ClonerType = null;
           
            // 
            // ImsWindowsFormsApplication
            // 
            this.ApplicationName = "IMS";
            this.Modules.Add(this.systemModule);
            this.Modules.Add(this.businessClassLibraryCustomizationModule1);
            this.Modules.Add(this.validationModule1);
            this.Modules.Add(this.systemWindowsFormsModule1);
            this.Modules.Add(this.validationWindowsFormsModule1);
            this.Modules.Add(this.conditionalAppearanceModule1);
            this.Modules.Add(securityModule1);
            this.Modules.Add(this.notificationsModule1);
            this.Modules.Add(this.reportsModuleV21);
            this.Modules.Add(this.cloneObjectModule1);
            this.Modules.Add(this.YsvCoreModule1);
            this.Modules.Add(this.fileAttachmentsWindowsFormsModule1);
            this.Modules.Add(this.notificationsWindowsFormsModule1);
            this.Modules.Add(this.reportsWindowsFormsModuleV21);
            this.Modules.Add(this.schedulerModuleBase1);
            this.Modules.Add(this.schedulerWindowsFormsModule1);
            this.Modules.Add(this.ysvWinModule1);
            this.Modules.Add(this.viewVariantsModule1);
            this.Modules.Add(this.coreModule);
            this.ResourcesExportedToModel.Add(typeof(DevExpress.ExpressApp.Win.Templates.MainRibbonFormV2TemplateLocalizer));
            this.ResourcesExportedToModel.Add(typeof(DevExpress.ExpressApp.Win.Localization.XtraEditorsLocalizer));
            this.ResourcesExportedToModel.Add(typeof(DevExpress.ExpressApp.Scheduler.Win.SchedulerControlLocalizer));
            this.ResourcesExportedToModel.Add(typeof(DevExpress.ExpressApp.Scheduler.SchedulerModuleBaseLocalizer));
            this.ResourcesExportedToModel.Add(typeof(DevExpress.ExpressApp.Win.Templates.DetailRibbonFormV2TemplateLocalizer));
            this.Security = this.securityStrategyComplex1;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        SystemModule systemModule;
        SystemWindowsFormsModule systemWindowsFormModule;
        App_Module coreModule;
        //        private WinApp.IMSWindowsFormsModule Module4;
        BusinessClassLibraryCustomizationModule businessClassLibraryCustomizationModule1;
        ValidationModule validationModule1;
        ValidationWindowsFormsModule validationWindowsFormsModule1;
        ConditionalAppearanceModule conditionalAppearanceModule1;
        SystemWindowsFormsModule systemWindowsFormsModule1;
        SecurityStrategyComplex securityStrategyComplex1;
        AuthenticationStandard authenticationStandard1;
        SchedulerWindowsFormsModule schedulerWindowsFormsModule1;
        SchedulerModuleBase schedulerModuleBase1;


        FileAttachmentsWindowsFormsModule fileAttachmentsWindowsFormsModule1;

        NotificationsWindowsFormsModule notificationsWindowsFormsModule1;
        NotificationsModule notificationsModule1;
        ReportsModuleV2 reportsModuleV21;
        ReportsWindowsFormsModuleV2 reportsWindowsFormsModuleV21;


        YsvCoreModule YsvCoreModule1;
        YsvWinModule ysvWinModule1;
        ViewVariantsModule viewVariantsModule1;
       
        private DevExpress.ExpressApp.CloneObject.CloneObjectModule cloneObjectModule1;
        
    }
}
